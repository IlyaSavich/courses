package homework_1.opencv;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.Videoio;

/**
 * Class for face detection
 * Making a photo by camera and recognize faces on it
 */
public class FaceDetector extends Thread {

    /**
     * The full path with the file name to the place where will be stored a photo by a camera
     */
    public static final String CAMERA_PHOTO_SAVE_FULL_PATH = System.getProperty("user.dir") + "/src/homework_1/opencv/camera.jpg";

    /**
     * The full path with the file name to the place where will be stored a modified photo with detected faces
     */
    public static final String FACE_DETECTED_PHOTO_FULL_PATH = System.getProperty("user.dir") + "/src/homework_1/opencv/faceDetection.png";

    /**
     * Making photo and recognize faces on it. If such files exists rewrite them
     */
    @Override
    public void run() {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

        makePhoto();
        recognize();
    }

    /**
     * Recognize faces on the photo
     */
    private void recognize() {
        // Create a face detector from the cascade file in the resources
        // directory.
        CascadeClassifier faceDetector = new CascadeClassifier(getClass().getResource("/homework_1/opencv/lbpcascade_frontalface.xml").getPath());
        Mat image = Imgcodecs.imread(FaceDetector.CAMERA_PHOTO_SAVE_FULL_PATH);
        // Detect faces in the image.
        // MatOfRect is a special container class for Rect.
        MatOfRect faceDetections = new MatOfRect();
        faceDetector.detectMultiScale(image, faceDetections);
        // Draw a bounding box around each face.
        for (Rect rect : faceDetections.toArray()) {
            Imgproc.rectangle(image, new Point(rect.x, rect.y), new Point(rect.x + rect.width, rect.y + rect.height), new Scalar(0, 255, 0));
        }
        // Save the visualized detection.
        Imgcodecs.imwrite(FaceDetector.FACE_DETECTED_PHOTO_FULL_PATH, image);
    }

    /**
     * Making photo by camera
     */
    private void makePhoto() {
        VideoCapture camera = new VideoCapture(0);
        camera.set(Videoio.CV_CAP_PROP_FRAME_WIDTH, 1280);
        camera.set(Videoio.CV_CAP_PROP_FRAME_HEIGHT, 720);

        if (!camera.isOpened()) {
            System.out.println("Error");
        } else {

            Mat frame = new Mat();
            if (camera.read(frame)) {
                Imgcodecs.imwrite(FaceDetector.CAMERA_PHOTO_SAVE_FULL_PATH, frame);
            }
        }
        camera.release();
    }
}
