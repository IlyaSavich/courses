package homework_1.occurrences;

import com.sun.deploy.util.StringUtils;

import java.util.LinkedList;
import java.util.List;

/**
 * Class for counting a substring occurrence count for each string in list
 * Uses simple foreach method to find occurrences
 */
public class ListOccurrences {

    /**
     * An occurrence will be found for this substring
     */
    private String substring;

    /**
     * The list of strings
     */
    private List<String> list;

    /**
     * Setting the list of strings and the substring
     *
     * @param list List<String>
     * @param substring String
     */
    public ListOccurrences(List<String> list, String substring) {
        this.list = list;
        this.substring = substring;
    }

    /**
     * Calculate substring occurrences for each string
     * Uses simple foreach method
     *
     * @return List <Integer>
     */
    public List<Integer> occurrences() {
        List<Integer> counts = new LinkedList<>();

        for (String string : this.list) {
            counts.add(string.split(this.substring).length - 1);
        }

        return counts;
    }
}
