package homework_1.iterators;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.security.InvalidParameterException;

public class Fibonacci {

    /**
     * Min value of order of fibonacci number
     */
    public static final Integer MIN_ORDER_LIMIT = 0;

    /**
     * The fibonacci value
     */
    private BigInteger value;

    /**
     * The fibonacci value order
     */
    private Integer order;

    /**
     * The new instance initialize
     * Init fibonacci by order
     * Check if the order is valid
     *
     * @param order Integer
     */
    public Fibonacci(Integer order) {
        if (order > Fibonacci.MIN_ORDER_LIMIT) {
            this.order = order;
            this.value = getValueByOrder();
        }
    }

    /**
     * The new instance initialize
     * Init fibonacci by his value
     * Check if the value is valid
     *
     * @param value BigInteger
     */
    public Fibonacci(BigInteger value) {
        if (isValid(value)) {
            this.value = value;
            this.order = getOrderByValue();
        }
    }

    /**
     * The new instance initialize
     * Init fibonacci by another fibonacci
     *
     * @param fibonacci Fibonacci
     */
    public Fibonacci(Fibonacci fibonacci) {
        this.value = fibonacci.value;
        this.order = fibonacci.order;
    }

    /**
     * Getting fibonacci value
     *
     * @return BigInteger Fibonacci value
     */
    public BigInteger getValue() {
        return this.value;
    }

    /**
     * Getting fibonacci value order
     *
     * @return Integer Fibonacci value order
     */
    public Integer getOrder() {
        return this.order;
    }

    /**
     * Setting fibonacci value
     * If the value is invalid throws exception
     * Else init and return the fibonacci value
     *
     * @throws InvalidParameterException
     * @param value BigInteger
     * @return BigInteger Fibonacci value
     */
    public BigInteger setValue(BigInteger value) {
        if (isValid(value)) {
            this.value = value;
            this.order = getOrderByValue();

            return this.value;
        }
        throw new InvalidParameterException();
    }

    /**
     * Setting fibonacci value order
     * If order is invalid throws exception
     * Else init and return the order
     *
     * @throws InvalidParameterException
     * @param order Integer
     * @return Integer Fibonacci value order
     */
    public Integer setOrder(Integer order) {
        if (order > Fibonacci.MIN_ORDER_LIMIT) {
            this.order = order;
            this.value = getValueByOrder();

            return this.order;
        }
        throw new InvalidParameterException();
    }

    /**
     * Setting fibonacci value and it's order
     *
     * @param fibonacci Fibonacci
     * @return Fibonacci Fibonacci instance
     */
    public Fibonacci setProperties(Fibonacci fibonacci) {
        this.value = fibonacci.value;
        this.order = fibonacci.order;

        return this;
    }

    /**
     * Calculating fibonacci value by it's order
     *
     * @return BigInteger Fibonacci value
     */
    public BigInteger getValueByOrder() {
        return new BigInteger(new BigDecimal((1 + Math.sqrt(5)) / 2)
                .pow(this.order)
                .add(new BigDecimal((1 - Math.sqrt(5)) / 2).pow(this.order)
                        .multiply(new BigDecimal(-1)))
                .divide(new BigDecimal(Math.sqrt(5)), 1)
                .setScale(0, RoundingMode.HALF_UP)
                .toString());
    }

    /**
     * Calculating fibonacci order by it's value
     * @return Integer Fibonacci value order
     */
    public Integer getOrderByValue() {
        return Helper.log(new BigDecimal(this.value)
                .multiply(new BigDecimal(Math.sqrt(5))))
                .divide(Helper.log(new BigDecimal((1 + Math.sqrt(5)) / 2)), 1)
                .setScale(0, RoundingMode.HALF_UP)
                .intValue();
    }

    /**
     * Check if the number is valid fibonacci value
     *
     * @param number BigInteger
     * @return boolean
     */
    public static Boolean isValid(BigInteger number) {
        BigInteger prev = new BigInteger("1");
        BigInteger fibonacci = new BigInteger("1");
        Integer compare;
        do {
            compare = fibonacci.compareTo(number);
            if (compare == 0) {
                return true;
            }

            BigInteger current = fibonacci;
            fibonacci = fibonacci.add(prev);
            prev = current;

        } while (compare == -1);

        return false;
    }
}
