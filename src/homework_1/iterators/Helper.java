package homework_1.iterators;

import java.math.BigDecimal;

public class Helper {

    /**
     * Calculating log of BigDecimal value
     *
     * @param bd BigDecimal
     * @return BigDecimal
     */
    public static BigDecimal log(BigDecimal bd) {
        double d = bd.doubleValue();
        double result = 0.0;
        result = Math.log(d);
        if (Double.isNaN(result) || Double.isInfinite(result))
            return new BigDecimal("0");
        BigDecimal ret = new BigDecimal(Math.log(d));
        return ret.setScale(bd.scale(), BigDecimal.ROUND_HALF_EVEN);
    }
}
