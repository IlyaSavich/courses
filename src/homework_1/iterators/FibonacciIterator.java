package homework_1.iterators;

import java.math.BigInteger;
import java.util.Iterator;

/**
 * Iterator on the fibonacci numbers
 * You can init iterator by value or order of fibonacci number
 */
public class FibonacciIterator implements Iterator<BigInteger> {

    /**
     * The current iteration value
     */
    private Fibonacci fibonacci;

    /**
     * The new instance initialize
     *
     * @param value BigInteger
     */
    public FibonacciIterator(BigInteger value) {
        this.fibonacci = new Fibonacci(value);
    }

    /**
     * The new instance initialize
     *
     * @param order Integer
     */
    public FibonacciIterator(Integer order) {
        this.fibonacci = new Fibonacci(order);
    }

    /**
     * Check if the iterator has next element
     * Always true because number has no up limit
     *
     * @return boolean
     */
    @Override
    public boolean hasNext() {
        return true;
    }

    /**
     * Making iteration step if there is a next element
     *
     * @return BigInteger stepping value
     */
    @Override
    public BigInteger next() {
        Fibonacci current = new Fibonacci(this.fibonacci);
        fibonacci.setOrder(fibonacci.getOrder() + 1);

        return current.getValue();
    }
}
