package homework_1.iterators;

import java.math.BigInteger;
import java.util.Iterator;

/**
 * Iterator on the squares of integers
 * You can init iterator by BigInteger or Integer
 */
public class SquareNumberIterator implements Iterator<BigInteger> {

    /**
     * The value of iteration step
     */
    private static final BigInteger STEP = new BigInteger("1");

    /**
     * The current iteration value
     * We will store in this variable int values without squares
     */
    private BigInteger cursor;

    /**
     * The new instance initialize
     *
     * @param value BigInteger
     */
    public SquareNumberIterator(BigInteger value) {
        this.cursor = value;
    }

    /**
     * The new instance initialize
     *
     * @param value BigInteger
     */
    public SquareNumberIterator(Integer value) {
        this.cursor = new BigInteger(value.toString());
    }

    /**
     * Check if the iterator has next element
     * Always true because number has no up limit
     *
     * @return boolean
     */
    @Override
    public boolean hasNext() {
        return true;
    }

    /**
     * Making iteration step if there is a next element
     *
     * @return BigInteger Current value
     */
    @Override
    public BigInteger next() {
        BigInteger current = this.cursor;

        if (hasNext()) {
            this.cursor = this.cursor.add(SquareNumberIterator.STEP);
        }

        return current.multiply(current);
    }
}
