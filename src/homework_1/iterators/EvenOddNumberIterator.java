package homework_1.iterators;

import java.math.BigInteger;
import java.util.Iterator;

/**
 * Iterator on the even and odd integers
 * You can init iterator by BigInteger or Integer
 */
public class EvenOddNumberIterator implements Iterator<BigInteger> {

    /**
     * The value of the iteration step
     */
    private static final BigInteger STEP = new BigInteger("2");

    /**
     * Current iteration value
     */
    private BigInteger cursor;

    /**
     * The new instance initialize
     *
     * @param value BigInteger
     */
    public EvenOddNumberIterator(BigInteger value) {
        this.cursor = value;
    }

    /**
     * The new instance initialize
     *
     * @param value Integer
     */
    public EvenOddNumberIterator(Integer value) {
        this.cursor = new BigInteger(value.toString());
    }

    /**
     * Check if the iterator has next element
     * Always true because number has no up limit
     *
     * @return boolean
     */
    @Override
    public boolean hasNext() {
        return true;
    }

    /**
     * Making iteration step if there is a next element
     *
     * @return BigInteger Current value
     */
    @Override
    public BigInteger next() {
        BigInteger current = this.cursor;

        if (hasNext()) {
            this.cursor = this.cursor.add(EvenOddNumberIterator.STEP);
        }

        return current;
    }
}
