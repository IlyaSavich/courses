package homework_1.RegEx;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Hashtable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class realize the determining of the best chatter. It's used as help to the Regular class
 */
class ChatterHelper {

    /**
     * The count of words of each chatter
     */
    private Hashtable<String, Integer> wordCount = new Hashtable<>();

    /**
     * The path to the file with text
     */
    private static String chatFilePath = System.getProperty("user.dir") + "/src/homework_1/RegEx/messages.txt";

    /**
     * You can set the path for the file.
     * But you must to follow the given structure of the submission of the chat:
     *
     * -->>ChatterHelper name: Text
     *
     * @param chatFilePath String Full path
     */
    public ChatterHelper(String chatFilePath) {
        if (!chatFilePath.equals("")) {
            ChatterHelper.chatFilePath = chatFilePath;
        }
    }

    /**
     * Determine the best chatter in the init chat file
     *
     * @return Hashtable <String, Integer> The best chatter with words count
     */
    public Hashtable<String, Integer> bestChatter() {
        calcWordsCount(readFile());

        return determineBestChatter();
    }

    /**
     * Calculate counts of words for each chatter
     *
     * @return Hashtable <String, Integer>: ChatterHelper name -> words count
     */
    private Hashtable calcWordsCount(String chat) {
        Matcher matcher = Pattern.compile("(-->>([\\p{L}]*\\s*):([\\p{L}0-9!\\p{Blank})(-,.?]+)*)")
                .matcher(chat);

        while (matcher.find()) {
            int count = wordCount.containsKey(matcher.group(2)) ? wordCount.get(matcher.group(2)) : 0;

            wordCount.put(matcher.group(2), count + countWord(matcher.group(3)));
        }

        return wordCount;
    }

    /**
     * Determine the best chatter by the counts of written words
     *
     * @return The best chatter with words count
     */
    private Hashtable<String, Integer> determineBestChatter() {
        int maxWords = 0;
        Hashtable<String, Integer> bestChatter = new Hashtable<>();
        Boolean equal = false;

        for (String name : wordCount.keySet()) {
            if (maxWords == wordCount.get(name)) {
                equal = true;
            }
            if (maxWords < wordCount.get(name)) {
                maxWords = wordCount.get(name);

                bestChatter.clear();
                bestChatter.put(name, wordCount.get(name));
                equal = false;
            }
        }

        return equal ? new Hashtable<String, Integer>(){{
            put("Нет победителя", 0);
        }} : bestChatter;
    }

    /**
     * Counting number of words in string using regular expression.
     */
    private int countWord(String string) {
        if (string == null) {
            return 0;
        }
        String input = string.trim();

        return input.isEmpty() ? 0 : input.split("([\\p{Space}]+[\\p{Punct}]*)+").length;
    }

    /**
     * Read text from file
     */
    private String readFile() {
        String text = "";

        try (BufferedReader fileOut = new BufferedReader(new FileReader(ChatterHelper.chatFilePath))) {

            for (String line; (line = fileOut.readLine()) != null; ) {
                text += line + " ";
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return text;
    }
}
