package homework_1.RegEx;

import java.util.Hashtable;
import java.util.regex.Pattern;

/**
 * Class for working with the RegEx
 */
public class Regular {

    /**
     * Validate email
     *
     * @param email String
     * @return Boolean Result of validation
     */
    public static Boolean email(String email) {
        email = clean(email);

        return Pattern
                .compile("^[A-Za-z][A-Za-z0-9!#$%&'*+—/=?^_`{|}~.]*@[A-Za-z0-9.]+[.][a-z]+")
                .matcher(email)
                .matches();
    }

    /**
     * Remove duplicate words
     * Removing words
     *
     * @param text String
     * @return Modified text
     */
    public static String removeDuplicate(String text) {
        text = clean(text);

        return text.replaceAll("\\b(\\w+)\\b(?=.*\\1)", "").trim();
//        return text.replaceAll("(?<=(?<dupl>\\b\\w+\\b)<insert here something>)\\k<dupl>", "").trim();
    }

    /**
     * Determining the best chatter
     * If you don't want to specify path of a chat file you can to pass in the method empty string,
     * in this case you receive the result from the test file
     *
     * @param chatFilePath String
     * @return The best chatter with words count
     */
    public static Hashtable<String, Integer> bestChatter(String chatFilePath) {
        return new ChatterHelper(chatFilePath).bestChatter();
    }

    /**
     * Cleaning the string from redundant spaces. You can add some new cleaning rules
     *
     * @param string String
     * @return Clean string
     */
    private static String clean(String string) {
        return string.trim();
    }
}
