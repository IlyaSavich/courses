package homework_1;

import homework_1.RegEx.Regular;
import homework_1.iterators.EvenOddNumberIterator;
import homework_1.iterators.FibonacciIterator;
import homework_1.iterators.SquareNumberIterator;
import homework_1.occurrences.ListOccurrences;
import homework_1.opencv.FaceDetector;
import homework_1.race.Player;
import homework_1.race.Race;

import java.util.LinkedList;
import java.util.List;

/**
 * Testing homework_1. Output in console
 */
public class TestHomework1 {

    public static void test() {
        showIterators();
        showOccurrenceCounting();
        showRegEx();
        showRace();
        showOpenCV();
    }

    /**
     * Make a race.
     * Showing the work of the Race class
     */
    private static void showRace() {
        System.out.println("\n\n#-------------- Race --------------#\n\n");

        Integer playersNumber = 10;

        Race race = new Race(playersNumber);

        Player winner = race.race();

        System.out.println("\n#-- WINNER --#\n");
        System.out.println(winner.getNumber() + " ~ " + winner.getPoints());
    }

    /**
     * Showing the work of the OpenCV library
     * By default the result in the /src/homework_1/opencv/faceDetection.png
     */
    private static void showOpenCV() {
        System.out.println("\n\n#-------------- OpenCV --------------#\n\n");

        FaceDetector detector = new FaceDetector();
        detector.start();
        while (detector.isAlive()) {
        } // waiting while detecting

        System.out.println("You can see the result in the /src/homework_1/opencv/faceDetection.png");
    }

    /**
     * Showing the work of the ListOccurrences class
     * Uses test data
     */
    private static void showOccurrenceCounting() {
        System.out.println("\n\n#-------------- Occurrence counting --------------#\n\n");

        List<String> list = new LinkedList<String>() {{
            add("Hello world oanoe");
            add("qwertoooyuiop");
            add("asdfghjkl");
            add("zxcvbnawefw");
        }};

        String substring = "o";

        ListOccurrences a = new ListOccurrences(list, substring);

        List<Integer> occurrences = a.occurrences();

        System.out.println("Substring -> " + substring);

        System.out.println("\n#-- Result --#\n");
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i) + " -> " + occurrences.get(i));
        }
    }

    /**
     * Showing the work of the Regular class
     * Uses test data
     */
    private static void showRegEx() {
        System.out.println("\n\n#-------------- RegEx --------------#\n\n");


        System.out.println("#-- email --#\n");

        System.out.println("\"ilia.savich.97@gmail.com\" -> " + Regular.email("ilia.savich.97@gmail.com"));
        System.out.println("\"ilia.savich.97@gmail.\" -> " + Regular.email("ilia.savich.97@gmail."));
        System.out.println("\"ilia.savich.97@gmail\" -> " + Regular.email("ilia.savich.97@gmail"));
        System.out.println("\"ilia.savich.97@.com\" -> " + Regular.email("ilia.savich.97@.com"));
        System.out.println("\".savich.97@gmail.com\" -> " + Regular.email(".savich.97@gmail.com"));
        System.out.println("\"0ilia.savich.97@gmail.com\" -> " + Regular.email("0ilia.savich.97@gmail.com"));
        System.out.println("\"ilia.savich.97gmail.com\" -> " + Regular.email("ilia.savich.97gmail.com"));


        System.out.println("\n#-- Duplicated words --#\n");

        String text = "Lorem - ipsum? dolor, sit. | amet amet sit dolor dolor ipsum Lorem";

        System.out.println("text -> " + text);
        System.out.println("After removing -> " + Regular.removeDuplicate(text));


        System.out.println("\n#-- Best chatter --#\n");

        System.out.println("Chat text in the /src/homework_1/RegEx/messages.txt");
        System.out.println("Best -> " + Regular.bestChatter("")); // uses the test file
    }

    /**
     * Showing the work of the iterators
     * Uses test data
     */
    private static void showIterators() {
        System.out.println("\n\n#-------------- Iterators --------------#\n\n");

        System.out.println("#-- Even odd number iterator --#\n");

        EvenOddNumberIterator evenOddNumberIterator = new EvenOddNumberIterator(1);

        for (int i = 0; i < 10; i++) {
            System.out.println(evenOddNumberIterator.next());
        }

        System.out.println("\n#-- Square number iterator --#\n");

        SquareNumberIterator squareNumberIterator = new SquareNumberIterator(1);

        for (int i = 0; i < 10; i++) {
            System.out.println(squareNumberIterator.next());
        }

        System.out.println("\n#-- Fibonacci iterator --#\n");

        FibonacciIterator fibonacciIterator = new FibonacciIterator(1);

        for (int i = 0; i < 10; i++) {
            System.out.println(fibonacciIterator.next());
        }
    }
}
