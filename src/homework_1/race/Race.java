package homework_1.race;

import java.security.InvalidParameterException;
import java.util.*;

/**
 * Class for organize the the race
 * Init players. Start race. Determine the winner
 */
public class Race {

    /**
     * Min value of players number
     */
    public static final int MIN_PLAYERS_NUMBER = 1;

    /**
     * The list of all players
     */
    private List<Player> players = new LinkedList<>();

    /**
     * Initialize the players
     *
     * @param playersNumber Integer
     */
    public Race(Integer playersNumber) {
        if (isValidPlayersNumber(playersNumber)) {

            for (int i = Race.MIN_PLAYERS_NUMBER; i <= playersNumber; i++) {
                this.players.add(new Player(i, playerSleepTime()));
            }

        } else {
            throw new InvalidParameterException();
        }
    }

    /**
     * Starting the race
     */
    public void start() {
        this.players.forEach(Thread::start);
    }

    /**
     * Do the race
     *
     * @return The winner of the race
     */
    public Player race() {
        this.start();

        return this.winner();
    }

    /**
     * Determine the winner by the max points
     * Wait if the race is not ended
     *
     * !! There is console output for debug. If you don't need this you can delete it
     *
     * @return Player Winner
     */
    public Player winner() {
        while (!isEnd()) {} //iterates while all players are not ended

        Integer maxPoints = 0;
        Player winner = new Player(100);

        System.out.println("#-- ALL PLAYERS --#\n");

        for (Player player : this.players) {

            System.out.println(player.getNumber() + " ~ " + player.getPoints());

            if (player.getPoints() > maxPoints) {
                winner = player;
                maxPoints = player.getPoints();
            }
        }

        return winner;
    }

    /**
     * Validate the player number
     *
     * @param playersNumber Integer
     * @return boolean
     */
    private boolean isValidPlayersNumber(Integer playersNumber) {
        return playersNumber >= Race.MIN_PLAYERS_NUMBER;
    }

    /**
     * Check is the race ended, it becomes when all players finishes
     *
     * @return boolean
     */
    private Boolean isEnd() {
        for (Player player : this.players) {
            if (!player.isFinished()) {
                return false;
            }
        }

        return true;
    }

    /**
     * Random value of player sleep time to add some random in race.
     * Magic number is heuristic for fast race and for making chance for players with big number
     *
     * @return Integer
     */
    private Integer playerSleepTime() {
        return (int) Math.round(30 * Math.random());
    }
}
