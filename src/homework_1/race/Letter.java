package homework_1.race;

import java.util.Iterator;

/**
 * Iterator by english letters
 * Gives letters for players
 */
public class Letter implements Iterator<Character> {

    /**
     * Used for determining when we iterate all uppercase letters to start iterate over the lowercase letters,
     * because between upper and lowercase there some symbols in ASCII
     */
    public static final char LAST_UPPERCASE_LETTER = 'Z';

    /**
     * Used for determining when we iterate all uppercase letters to start iterate over the lowercase letters,
     * because between upper and lowercase there some symbols in ASCII
     */
    public static final char FIRST_LOWERCASE_LETTER = 'a';

    /**
     * Used for determining the last letter and end of iteration
     */
    public static final char LAST_LOWERCASE_LETTER = 'z';

    /**
     * The iterate value
     */
    private Character value = 'A';

    /**
     * Check the end of letters
     *
     * @return boolean Is there any else letters
     */
    @Override
    public boolean hasNext() {
        return this.value < Letter.LAST_LOWERCASE_LETTER;
    }

    /**
     * Getting next letter
     * If there is no next letter return current without making step
     * If there is next letter check if we finishing with uppercase to start iterate by lowercase letters
     *
     * @return char Current letter
     */
    public Character next() {
        Character current = this.value;

        if (hasNext()) {

            if (isUpperEnd()) {
                this.value = Letter.FIRST_LOWERCASE_LETTER;
            } else {
                this.value++;
            }

        }

        return current;
    }

    /**
     * Check if we came to end uppercase
     *
     * @return boolean
     */
    private boolean isUpperEnd() {
        return this.value == Letter.LAST_UPPERCASE_LETTER;
    }
}
