package homework_1.race;

import java.security.InvalidParameterException;

/**
 * Class implements players for the race.
 * Each player for each step takes letter and search it in the text, if find
 * cutting in text finding letter and continue searching. If not found then takes next letter.
 * End race when steps over all letters
 */
public class Player extends Thread {

    /**
     * Used for validate player number
     */
    public static final int MIN_PLAYER_NUMBER = 1;

    /**
     * Used for validate sleep time
     */
    public static final int MIN_SLEEP_TIME = 0;

    /**
     * There storing the player result
     * Points will be increased for each letter that the player will find in text
     * Winner determined by his points
     * Starting with zero points
     */
    private Integer points = 0;

    /**
     * The player number
     */
    private Integer number;

    /**
     * Time in ms in which the player will wait after successful cutting
     */
    private Integer sleepTime = 1500;

    /**
     * Saying is the player finished the race
     */
    private Boolean finish = false;

    /**
     * There storing current letter that the player must find in text
     */
    private Letter letter;

    /**
     * Players will cutting letters from this text
     */
    private Text text;

    /**
     * The new player instance
     *
     * @param number Integer The player number
     */
    public Player(Integer number) {
        this.letter = new Letter();
        this.text = Text.getInstance();
        if (isValidNumber(number)) {
            this.number = number;
        } else {
            throw new InvalidParameterException();
        }
    }

    /**
     * The new player instance
     *
     * @param number Integer The player number
     * @param sleepTime Integer The player sleep time after successful cutting
     */
    public Player(Integer number, Integer sleepTime) {
        this.letter = new Letter();
        this.text = Text.getInstance();

        if (isValidNumber(number)) {
            this.number = number;
        } else {
            throw new InvalidParameterException();
        }

        if (isValidSleepTime(sleepTime)) {
            this.sleepTime = sleepTime;
        } else {
            throw new InvalidParameterException();
        }
    }

    /**
     * Method implements player participation in a race
     * Stopping the race if the player passed by all letters
     * When finished say this
     */
    @Override
    public void run() {
        while (letter.hasNext()) {

            try {
                cut();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        this.finish = true;
    }

    /**
     * Cutting current letter from the text and getting next letter
     *
     * @throws InterruptedException
     */
    public void cut() throws InterruptedException {
        char currentLetter = letter.next();

        while (this.text.find(currentLetter)) {
            this.points++;
            Thread.sleep(sleepTime);
        }
    }

    /**
     * Getting the player points
     *
     * @return Integer
     */
    public Integer getPoints() {
        return this.points;
    }

    /**
     * Getting the player number
     *
     * @return Integer
     */
    public Integer getNumber() {
        return this.number;
    }

    /**
     * Check is the player finished the race
     * @return boolean
     */
    public boolean isFinished() {
        return this.finish;
    }

    /**
     * Validate the player number
     *
     * @param number Integer
     * @return boolean
     */
    private boolean isValidNumber(Integer number) {
        return number >= Player.MIN_PLAYER_NUMBER;
    }

    /**
     * Validate the player sleep time
     *
     * @param time Integer
     * @return boolean
     */
    private boolean isValidSleepTime(Integer time) {
        return time >= Player.MIN_SLEEP_TIME;
    }
}