package homework_1.race;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * The text for a race. Singleton implementation
 * Implements reading text from file and cutting letterfrom text
 */
public class Text {

    /**
     * The path to the file with text
     */
    public static String textFilePath = System.getProperty("user.dir") + "/src/homework_1/race/text.txt";

    /**
     * Index check if a letter not found in the text
     */
    public static final int INDEX_NOT_FOUND = -1;

    /**
     * Variable store the text
     */
    private StringBuilder text = new StringBuilder("");

    /**
     * The text instance
     */
    private static volatile Text instance;

    /**
     * The new instance initialize. Read text from file
     */
    private Text() {
        try {

            readFile();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Getting singleton instance
     *
     * @return Text Instance
     */
    public static Text getInstance() {
        if (instance == null) {
            synchronized (Text.class) {
                if (instance == null) {
                    instance = new Text();
                }
            }
        }

        return instance;
    }

    /**
     * Finding a letter in the text. If success then cutting it else do nothing
     * Return the result of searching
     *
     * @param letter Character
     * @return boolean
     */
    public synchronized boolean find(Character letter) {
        int indexDelete = this.text.indexOf(letter.toString());

        if (indexDelete != Text.INDEX_NOT_FOUND) {
            this.text.deleteCharAt(indexDelete);

            return true;
        }

        return false;
    }

    /**
     * Read text from file
     *
     * @throws IOException
     */
    private void readFile() throws IOException {
        FileInputStream in = null;

        try {
            in = new FileInputStream(Text.textFilePath);

            int character;
            while ((character = in.read()) != -1) {
                this.text.append((char) character);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        if (in != null) {
            in.close();
        }
    }
}
